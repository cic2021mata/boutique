/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.mycompany.boutique.Personne;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author toma
 */
public class PersonneService {
    
    static List<Personne> liste;
    
    // Ajouter une personne
    public void ajouter(Personne personne) {
        liste.add(personne);
    }
    
    // Modifier une personne
    public void modifier(Personne e) {
        for(Personne personne : liste) {
            if(personne.getId().equals(e.getId())) {
                liste.set(liste.indexOf(personne), e);
                return;
            }
        }
    }
    
    // Trouver une personne
    public Personne trouver(Integer id) {
        Personne find = new Personne();
        
        for(Personne personne : liste) {
            if(personne.getId().equals(id)) {
                find = personne;
                break;
            }
        }
        return find;
    }
    
    // Supprimer Une personne à part de l'id
    public void supprimer(Integer id) {
        for(Personne personne : liste) {
            if(personne.getId().equals(id)) {
                liste.remove(personne);
                break;
            }
        }
    }
    
    // Supprimer une personne donnée
    public void supprimer(Personne personne) {
        liste.remove(personne);
    }
    
    // Lister les personne.
    public List<Personne> lister() {
        return liste;
    }
    
    // Lister à partir d'un nombre
    public List<Personne> lister(int debut, int nombre) {
        List<Personne> element = new LinkedList<>();
        for(int i = debut - 1; i < nombre; i++ ) {
            element.add(liste.get(i));
        }
        return element;
    }
    
}
