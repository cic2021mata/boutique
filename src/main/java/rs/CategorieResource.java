/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.mycompany.boutique.Categorie;
import java.util.List;
import javax.ws.rs.GET;


import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.CategorieService;

/**
 *
 * @author toma
 */
@Path("/categorie")
public class CategorieResource {
    
    CategorieService categorieService = new CategorieService();
    
    @POST
    public void ajouter(@QueryParam("e") Categorie e){
        categorieService.ajouter(e);
    }
    
    @POST
    public void modifier(@QueryParam("e") Categorie e) {
        categorieService.modifier(e);
    }
    
    @GET
    public Categorie trouver(@QueryParam("id") Integer id){
        return categorieService.trouver(id);
    }
    
    @POST
    public void supprimer(@QueryParam("id") Integer id) {
        categorieService.supprimer(id);
    }
    
    @POST 
    public void supprimer(@QueryParam("e") Categorie e) {
        categorieService.supprimer(e);
    }
    
    @GET
    public List<Categorie> lister() {
        return categorieService.lister();
    }
    
    @GET 
    public List<Categorie> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return categorieService.lister(debut, nombre);
    }    
}
