/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.mycompany.boutique.Employee;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.EmployeService;

/**
 *
 * @author toma
 */
@Path("/employe")
public class EmployeResource {
    
    EmployeService employeService;
    
    @POST
    public void ajouter(@QueryParam("employe") Employee employe)  {
        employeService.ajouter(employe);
    }
    
    @POST
    public void modifier(@QueryParam("employe") Employee employe) {
        employeService.modifier(employe);
    }
    
    @GET
    public Employee trouver(@QueryParam("id") Integer id) {
        return employeService.trouver(id);
    }
    
    @POST 
    public void supprimer(@QueryParam("id") Integer id) {
        employeService.supprimer(id);
    }
    
    @POST
    public void supprimer(@QueryParam("employe") Employee employe) {
        employeService.supprimer(employe);
    }
    
    @GET
    public List<Employee> lister() {
        return employeService.lister();
    }
    
    @GET
    public List<Employee> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return employeService.lister(debut, nombre);
    }
    
    
}
