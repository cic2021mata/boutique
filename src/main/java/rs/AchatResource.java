/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.mycompany.boutique.Achat;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.AchatService;

/**
 *
 * @author toma
 */
@Path("/achat")
public class AchatResource {
    
    AchatService achatService;
    
    @POST
    public void ajouter(@QueryParam("achat") Achat achat) {
        achatService.ajouter(achat);
    }
    
    @POST
    public void modifier(@QueryParam("achat") Achat achat){
        achatService.modifier(achat);
    }
    
    @GET 
    public Achat trouver(@QueryParam("id") Integer id) {
        return achatService.trouver(id);
    }
    
    @POST
    public void supprimer(@QueryParam("id") Integer id) {
        achatService.supprimer(id);
    }
    
    @POST
    public void supprimer(@QueryParam("achat") Achat achat) {
        achatService.supprimer(achat);
    }
    
    @GET 
    public List<Achat> lister() {
        return achatService.lister();
    }
    
    @GET
    public List<Achat> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return achatService.lister(debut, nombre);
    }
    
    
}
