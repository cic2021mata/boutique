/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.mycompany.boutique.Achat;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author toma
 */
public class AchatService {
    
    static List<Achat> liste;

    public AchatService() {
    }
    
    // Ajouter un Achat
    public void ajouter(Achat achat) {
        liste.add(achat);
    }
    
    // Modifier un Achat
    public void modifier(Achat e) {
        for(Achat achat : liste) {
            if(achat.getId().equals(e.getId())){
                liste.set(liste.indexOf(achat), e);  
                break;
            } 
        }    
    }
    
    //Trouver un achat
    public Achat trouver(Integer id) {
        Achat find = new Achat();
        for(Achat achat : liste) {
            if(achat.getId().equals(id)) {
                find = achat;
                break;
            }
        }
        return find;
    }
    
    // Supprimer avec id
    public void supprimer(Integer id) {
        for(Achat achat : liste) {
            if(achat.getId().equals(id)) {
                liste.remove(achat);
                return;
            }
        }
    }
    
    // Supprimer avec la classe
    public void supprimer(Achat achat) {
        liste.remove(achat);
    }
    
    // Lister les Achats.
    public List<Achat> lister() {
        return liste;
    }
    
    //Lister à partir de début un nombre d'achat
    public List<Achat> lister(int debut, int nombre) {
        List<Achat> element = new LinkedList<>();
        for(int i = debut - 1; i < nombre; i++) {
            element.add(liste.get(i));
        }
        return element;
    }
    
}
