/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.mycompany.boutique.Categorie;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author toma
 */
public class CategorieService {
    
    public static List<Categorie> liste;
    
    public void ajouter (Categorie e) {
        liste.add(e);
    }
    
    public void modifier(Categorie e) {
        for (Categorie categorie : liste){
            if(categorie.getId().equals(e.getId())){
                categorie = e;
                break;
            }
        }
    }
    
    public Categorie trouver(Integer id) {
        return liste.get(id);
    }
    
    public void supprimer(Categorie e) {
        liste.remove(e);
    }
    
    public void supprimer(Integer id) {
        liste.remove(id);
    }
    
    public List<Categorie> lister() {
        return liste;
    }
    
    public List<Categorie> lister(int debut, int nombre) {
        List<Categorie> elements = new LinkedList<Categorie>();
        
        for(int i = debut -1; i < nombre; i++){
            elements.add(liste.get(i));
        }
        
        return elements;
    }
    
    
    
}
