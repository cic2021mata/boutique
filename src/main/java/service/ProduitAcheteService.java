/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.mycompany.boutique.ProduitAchete;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author toma
 */
public class ProduitAcheteService {
    
    static List<ProduitAchete> liste;
    
    // Ajouter
    public void ajouter(ProduitAchete produitAchete) {
        liste.add(produitAchete);
    }
    
    // modifier
    public void modifier(ProduitAchete produitAchete) {
        liste.set(liste.indexOf(produitAchete), produitAchete);
    }
    
    // Supprimer
    public void supprimer(ProduitAchete produitAchete) {
        liste.remove(produitAchete);
    }
    
    // Lister les produits achtetés
    public List<ProduitAchete> lister() {
        return liste;
    }
    
    // Liste les produits acheté à partir d'un nombre
    public List<ProduitAchete> lister(int debut, int nombre) {
        List<ProduitAchete> elements = new LinkedList<>();
        for(int i = debut - 1; i < nombre; i++) {
            elements.add(liste.get(i));
                   
        }
        
        return elements;
    }
    
    
}
