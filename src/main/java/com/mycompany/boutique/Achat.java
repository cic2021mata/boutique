/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author toma
 */
public class Achat {
    private final static AtomicLong count = new AtomicLong(0);
    private final Long id;
    private double remise = 0;
    private final LocalDate dateAchat;
    private ArrayList<ProduitAchete> listProdAchete;
            
    
    public Achat(Long id, double remise, LocalDate dateAchat) {
        this.id = count.incrementAndGet();
        this.remise = remise;
        this.dateAchat = dateAchat;
        this.listProdAchete = new ArrayList<ProduitAchete>();
    }
    
    
    // Les Methodes
    
    public double getRemiseTotale() {
        for(int i = 0; i < this.listProdAchete.size(); i++) {
            this.remise += this.listProdAchete.get(i).getRemise();
        }
        return this.remise;
    }
    
    public ArrayList<ProduitAchete> getProdAchete() {return  this.listProdAchete;}
    public void setProdAchete( ProduitAchete prodAchat) { this.listProdAchete.add(prodAchat); }
    
    public double getPrixTotal() {
        double prix = 0;
        for(int i = 0; i < this.listProdAchete.size(); i++) {
            prix += this.listProdAchete.get(i).getPrixTotal();
        }
        return prix;
    }
    
    public LocalDate geDateAchat() {return this.dateAchat;}
    public Long getId() { return this.id;}
    
    // Redéfinition des méthodes
    @Override
    public String toString() {
        return "\nRemise Totale : "+this.getRemiseTotale()*100+" %"+"\nPrix Total : "+this.getPrixTotal()+"\nDate d'Achat : "+this.dateAchat;
    }
    
    public int hashCode() {
        int hash = 1;
        hash = hash * 12 + this.id.hashCode();
        hash = hash * 22 + (int)(hash * 14 + this.remise);
        hash = hash * 20 + this.dateAchat.hashCode();
        hash = hash * 3 + this.listProdAchete.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat other = (Achat) obj;
        if (Double.doubleToLongBits(this.remise) != Double.doubleToLongBits(other.remise)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.dateAchat, other.dateAchat)) {
            return false;
        }
        if (!Objects.equals(this.listProdAchete, other.listProdAchete)) {
            return false;
        }
        return true;
    }
    
}
