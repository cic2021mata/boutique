/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.mycompany.boutique.Client;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.ClientService;

/**
 *
 * @author toma
 */
@Path("/client")
public class ClientResource {
    
    ClientService clientService;
    
    @POST
    public void ajouter(@QueryParam("client") Client client) {
        clientService.ajouter(client);
    }
    
    @POST
    public void modifier(@QueryParam("client") Client client) {
        clientService.modifier(client);
    }
    
    @GET
    public Client trouver(@QueryParam("id") Integer id) {
        return clientService.trouver(id);
    }
    
    @POST
    public void supprimer(@QueryParam("id") Integer id) {
        clientService.supprimer(id);
    }
    
    @POST
    public void supprimer(@QueryParam("client") Client client) {
        clientService.supprimer(client);
    }
    
    @GET
    public List<Client> lister() {
        return clientService.lister();
    }
    
    @GET
    public List<Client> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return clientService.lister(debut, nombre);
    }
    
}
