/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.mycompany.boutique.ProduitAchete;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import service.ProduitAcheteService;

/**
 *
 * @author toma
 */
@Path("/produitachete")
public class ProduitAcheteResource {
 
    ProduitAcheteService produitAcheteService;
    
    @POST
    public void ajouter(@QueryParam("produitachete") ProduitAchete produitAchete) {
        produitAcheteService.ajouter(produitAchete);
    }
    
    @POST
    public void modifier(@QueryParam("produitachete") ProduitAchete produitAchete) {
        produitAcheteService.modifier(produitAchete);
    }
    
    @POST
    public void supprimer(@QueryParam("produitachete") ProduitAchete produitAchete) {
        produitAcheteService.supprimer(produitAchete);
    }
    
    @GET
    public List<ProduitAchete> lister() {
        return produitAcheteService.lister();
    }
    
    @GET
    public List<ProduitAchete> lister(@QueryParam("debut") int debut, @QueryParam("nombre") int nombre) {
        return produitAcheteService.lister(debut, nombre);
    }
}
