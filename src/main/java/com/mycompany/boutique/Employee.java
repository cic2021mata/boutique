/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.boutique;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author toma
 */
public class Employee extends Personne{
    
    private String cnss;
    private LocalDate dateEmbauche;
    private ArrayList<Achat> listAchat;
    
    
    public Employee(String nom, String prenom, LocalDate dateNaissance,String cnss) {
        super(nom, prenom, dateNaissance);
    }
    
    public String getCnss(){ return this.cnss;}
    public void setCnss(String cnss) {this.cnss = cnss;}
    
    public LocalDate getDateEmbauche(){ return this.dateEmbauche;}
    public void setDateEmbauche(LocalDate dateEmbauche) {this.dateEmbauche = dateEmbauche;}
    
    public ArrayList<Achat> getListAchat() { return this.listAchat; }
    
    // Redéfintition des methodes
    @Override
    public int hashCode(){
        int hash = 1;
        hash = hash * 3 + super.hashCode();
        hash = hash * 6 + this.cnss.hashCode();
        hash = hash * 9 + this.dateEmbauche.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (!Objects.equals(this.cnss, other.cnss)) {
            return false;
        }
        if (!Objects.equals(this.dateEmbauche, other.dateEmbauche)) {
            return false;
        }
        return true;
    }
    
    public String toString() {
        return super.toString()+"\nCnss : "+this.cnss+"\nDate d'Embauche : "+this.dateEmbauche;
    }
    
}
